/*
 * JavaScript file for the application to demonstrate
 * using the API for the People SPA
 */
$.ajaxPrefilter(function (opts, originalOpts, jqXHR) {
    // you could pass this option in on a "retry" so that it doesn't
    // get all recursive on you.
    if (opts.refreshRequest) {
        return;
    }

    // our own deferred object to handle done/fail callbacks
    var dfd = $.Deferred();

    // if the request works, return normally
    jqXHR.done(dfd.resolve);

    // if the request fails, do something else
    // yet still resolve
    jqXHR.fail(function () {
        var args = Array.prototype.slice.call(arguments);
        var token = localStorage.getItem('refresh_token');
        if (jqXHR.status === 401) {
            $.ajax({
                url: '/auth/refresh',
                headers: { "Authorization": 'Bearer ' + token },
                method: 'POST',
                refreshRequest: true,
                error: function () {
                    // reject with the original 401 data and then redirect to login page.
                    setTimeout(function () {
                        window.location = "/auth/login";
                    }, 4000);
                    dfd.rejectWith(jqXHR, args);
                },
                success: function (data) {
                    localStorage.setItem('access_token', data.access_token)
                    // retry with a copied originalOpts with new access token.
                    var newOpts = $.extend({}, originalOpts, { url: opts.url,  headers: { "Authorization": 'Bearer ' + data.access_token } });

                    // pass this one on to our deferred pass or fail.
                    $.ajax(newOpts).then(dfd.resolve, dfd.reject);
                }
            });

        } else {
            dfd.rejectWith(jqXHR, args);
        }
    })

    return dfd.promise(jqXHR);
});


// Create the namespace instance
let ns = {};

// Create the model instance
ns.model = (function () {
    'use strict';
    var token = localStorage.getItem('access_token');
    // Return the API
    return {
        read_one: function (user_id) {
            let ajax_options = {
                type: 'GET',
                url: `/api/users/${user_id}`,
                accepts: 'application/json',
                headers: { "Authorization": 'Bearer ' + token },
                dataType: 'json'
            };
            return $.ajax(ajax_options);
        },
        read: function () {
            let ajax_options = {
                type: 'GET',
                url: '/api/users',
                accepts: 'application/json',
                headers: { "Authorization": 'Bearer ' + token },
                dataType: 'json'
            };
            return $.ajax(ajax_options);
        },
        create: function (user) {
            let ajax_options = {
                type: 'POST',
                url: '/api/users',
                accepts: 'application/json',
                headers: { "Authorization": 'Bearer ' + token },
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(user)
            };
            return $.ajax(ajax_options);
        },
        update: function (user) {
            let ajax_options = {
                type: 'PUT',
                url: `/api/users/${user.id}`,
                accepts: 'application/json',
                headers: { "Authorization": 'Bearer ' + token },
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(user)
            };
            return $.ajax(ajax_options);
        },
        'delete': function (user_id) {
            let ajax_options = {
                type: 'DELETE',
                url: `/api/users/${user_id}`,
                accepts: 'application/json',
                headers: { "Authorization": 'Bearer ' + token },
                contentType: 'plain/text'
            };
            return $.ajax(ajax_options);
        }
    };
}());

// Create the view instance
ns.view = (function () {
    'use strict';

    const NEW_NOTE = 0,
        EXISTING_NOTE = 1;

    let $user_id = $('#user_id'),
        $first_name = $('#first_name'),
        $last_name = $('#last_name'),
        $email = $('#email'),
        $password = $('#password'),
        $create = $('#create'),
        $update = $('#update'),
        $delete = $('#delete'),
        $reset = $('#reset');

    // return the API
    return {
        NEW_NOTE: NEW_NOTE,
        EXISTING_NOTE: EXISTING_NOTE,
        reset: function () {
            $user_id.text('');
            $last_name.val('');
            $email.val('');
            $password.val('');
            $first_name.val('').focus();
        },
        update_editor: function (user) {
            $user_id.text(user.id);
            $last_name.val(user.last_name);
            $email.val(user.email);
            $first_name.val(user.first_name).focus();
        },
        set_button_state: function (state) {
            if (state === NEW_NOTE) {
                $create.prop('disabled', false);
                $update.prop('disabled', true);
                $delete.prop('disabled', true);
            } else if (state === EXISTING_NOTE) {
                $create.prop('disabled', true);
                $update.prop('disabled', false);
                $delete.prop('disabled', false);
            }
        },
        build_table: function (users) {
            let source = $('#users-table-template').html(),
                template = Handlebars.compile(source),
                html;

            // clear the table
            $('.users table > tbody').empty();

            // did we get a people array?
            if (users) {

                // Create the HTML from the template and people
                html = template({users: users})

                // Append the html to the table
                $('table').append(html);
            }
        },
        error: function (error_msg) {
            $('.error')
                .text(error_msg)
                .css('visibility', 'visible');
            setTimeout(function () {
                $('.error').fadeOut();
            }, 2000)
        }
    };
}());

// Create the controller
ns.controller = (function (m, v) {
    'use strict';

    let model = m,
        view = v,
        $url_user_id = $('#url_user_id'),
        $user_id = $('#user_id'),
        $first_name = $('#first_name'),
        $last_name = $('#last_name'),
        $email = $('#email'),
        $password = $('#password');

    // Get the data from the model after the controller is done initializing
    setTimeout(function () {
        view.reset();
        model.read()
            .done(function(data) {
                view.build_table(data);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                error_handler(xhr, textStatus, errorThrown);
            })

        if ($url_user_id.val() !== "") {
            model.read_one(parseInt($url_user_id.val()))
                .done(function(data) {
                    view.update_editor(data);
                    view.set_button_state(view.EXISTING_NOTE);
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    error_handler(xhr, textStatus, errorThrown);
                });
        }
    }, 100)

    // generic error handler
    function error_handler(xhr, textStatus, errorThrown) {
        let error_msg = `${textStatus}: ${errorThrown} - ${xhr.responseJSON.detail}`;

        view.error(error_msg);
        console.log(error_msg);
    }
    // initialize the button states
    view.set_button_state(view.NEW_NOTE);

    // Validate input
    function validate(first_name, last_name, email, password) {
        return first_name !== "" && last_name !== "" && email !== "" && password !== "";
    }

    // Create our event handlers
    $('#create').click(function (e) {
        let first_name = $first_name.val(),
            last_name = $last_name.val(),
            email = $email.val(),
            password = $password.val();

        e.preventDefault();

        if (validate(first_name, last_name, email, password)) {
            model.create({
                'first_name': first_name,
                'last_name': last_name,
                'email': email,
                'password': password
            })
                .done(function(data) {
                    model.read()
                        .done(function(data) {
                            view.build_table(data);
                        })
                        .fail(function(xhr, textStatus, errorThrown) {
                            error_handler(xhr, textStatus, errorThrown);
                        });
                    view.set_button_state(view.NEW_NOTE);
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    error_handler(xhr, textStatus, errorThrown);
                });

            view.reset();

        } else {
            alert('Fields are required.');
        }
    });

    $('#update').click(function (e) {
        let user_id = parseInt($user_id.text()),
            first_name = $first_name.val(),
            last_name = $last_name.val(),
            email = $email.val(),
            password = $password.val();

        e.preventDefault();

        if (validate(first_name, last_name, email, password)) {
            model.update({
                'id': user_id,
                'first_name': first_name,
                'last_name': last_name,
                'email': email,
                'password': password
            })
                .done(function(data) {
                    model.read()
                        .done(function(data) {
                            view.build_table(data);
                        })
                        .fail(function(xhr, textStatus, errorThrown) {
                            error_handler(xhr, textStatus, errorThrown);
                        });
                    view.reset();
                    view.set_button_state(view.NEW_NOTE);
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    error_handler(xhr, textStatus, errorThrown);
                })

        } else {
            alert('Fields are required.');
        }
        e.preventDefault();
    });

    $('#delete').click(function (e) {
        let user_id = parseInt($user_id.text());

        e.preventDefault();

        if (user_id) {
            model.delete(user_id)
                .done(function(data) {
                    model.read()
                        .done(function(data) {
                            view.build_table(data);
                        })
                        .fail(function(xhr, textStatus, errorThrown) {
                            error_handler(xhr, textStatus, errorThrown);
                        });
                    view.reset();
                    view.set_button_state(view.NEW_NOTE);
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    error_handler(xhr, textStatus, errorThrown);
                });

        }
    });

    $('#reset').click(function () {
        view.reset();
        view.set_button_state(view.NEW_NOTE);
    })

    $('table').on('click', 'tbody tr', function (e) {
        let $target = $(e.target).parent(),
            user_id = $target.data('id'),
            first_name = $target.data('first_name'),
            last_name = $target.data('last_name'),
            email = $target.data('email');

        view.update_editor({
            id: user_id,
            first_name: first_name,
            last_name: last_name,
            email: email
        });
        view.set_button_state(view.EXISTING_NOTE);
    });

}(ns.model, ns.view));


