from flask import render_template
from flask import jsonify, request
from flask_jwt_extended import (
    jwt_required, jwt_refresh_token_required, get_jwt_identity,
    create_refresh_token, create_access_token, get_jti,
    get_raw_jwt
)

import config
from models import User


# Get the application instance
connex_app = config.connex_app
connex_app.add_api("swagger.yml")

revoked_store = config.revoked_store


@connex_app.route("/")
def home():
    return render_template("home.html")


@connex_app.route("/auth/login", methods=['GET'])
def login_form():
    return render_template("login.html")


@connex_app.route('/auth/login', methods=['POST'])
def login_user():
    email = request.json.get('email', None)
    password = request.json.get('password', None)
    if email != None and password != None:
        users = User.query.filter(User.email==email)
        if users:
            user = users[0]
            if config.bcrypt.check_password_hash(user.password, password):
                access_token = create_access_token(identity=user.id)
                refresh_token = create_refresh_token(identity=user.id)

                access_jti = get_jti(encoded_token=access_token)
                refresh_jti = get_jti(encoded_token=refresh_token)
                revoked_store.set(access_jti, 'false', config.ACCESS_EXPIRES * 1.2)
                revoked_store.set(refresh_jti, 'false', config.REFRESH_EXPIRES * 1.2)

                ret = {
                    'access_token': access_token,
                    'refresh_token': refresh_token
                }
                return jsonify(ret), 200
    return jsonify({"msg": "Bad username or password"}), 401


@connex_app.route('/auth/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    user_id = get_jwt_identity()
    access_token = create_access_token(identity=user_id)
    access_jti = get_jti(encoded_token=access_token)
    revoked_store.set(access_jti, 'false', config.ACCESS_EXPIRES * 1.2)
    ret = {'access_token': access_token}
    return jsonify(ret), 200


@connex_app.route('/auth/access_revoke', methods=['DELETE'])
@jwt_required
def logout():
    jti = get_raw_jwt()['jti']
    revoked_store.set(jti, 'true', config.ACCESS_EXPIRES * 1.2)
    return jsonify({"msg": "Access token revoked"}), 200


# Endpoint for revoking the current users refresh token
@connex_app.route('/auth/refresh_revoke', methods=['DELETE'])
@jwt_refresh_token_required
def logout2():
    jti = get_raw_jwt()['jti']
    revoked_store.set(jti, 'true', config.REFRESH_EXPIRES * 1.2)
    return jsonify({"msg": "Refresh token revoked"}), 200


@connex_app.route("/users")
def users():
    return render_template("users.html")


if __name__ == "__main__":
    connex_app.run(debug=True)
