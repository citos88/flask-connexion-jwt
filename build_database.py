import os
from datetime import datetime
from config import db
from models import User
from config import bcrypt

# Data to initialize database with
USERS = [
    {
        "email": "user1@example.com",
        "password": "Qwerty123",
        "first_name": "User1",
        "last_name": "Example"
    },
    {
        "email": "user2@example.com",
        "password": "Qwerty123",
        "first_name": "User2",
        "last_name": "Example"
    },
]

# Delete database file if it exists currently
if os.path.exists("database.db"):
    os.remove("database.db")

# Create the database
db.create_all()

# iterate over the PEOPLE structure and populate the database
for u in USERS:
    user = User(
            email=u.get('email'),
            first_name=u.get('first_name'),
            last_name=u.get('last_name'),
            password=bcrypt.generate_password_hash(u.get('password')))

    db.session.add(user)

db.session.commit()
