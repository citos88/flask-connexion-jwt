import os
from datetime import timedelta
import connexion
import redis
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager
from flask_bcrypt import Bcrypt
from flask_migrate import Migrate
from flask_script import Manager

basedir = os.path.abspath(os.path.dirname(__file__))


connex_app = connexion.App(__name__, specification_dir=basedir)
app = connex_app.app


sqlite_url = "sqlite:////" + os.path.join(basedir, "database.db")
ACCESS_EXPIRES = timedelta(minutes=15)
REFRESH_EXPIRES = timedelta(days=30)

app.config["SQLALCHEMY_ECHO"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = sqlite_url
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['JWT_SECRET_KEY'] = 'super-secret-madafaka'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = ACCESS_EXPIRES
app.config['JWT_REFRESH_TOKEN_EXPIRES'] = REFRESH_EXPIRES
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']


db = SQLAlchemy(app)
migrate = Migrate(app, db)
manager = Manager(app)
jwt = JWTManager(app)
bcrypt = Bcrypt(app)
ma = Marshmallow(app)
revoked_store = redis.StrictRedis(host='localhost', port=6379, db=0, decode_responses=True)


@jwt.token_in_blacklist_loader
def check_if_token_is_revoked(decrypted_token):
    jti = decrypted_token['jti']
    entry = revoked_store.get(jti)
    if entry is None:
        return True
    return entry == 'true'