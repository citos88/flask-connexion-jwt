from flask import make_response, abort
from config import db
from models import User, UserSchema
from flask_jwt_extended import (
    jwt_required
)
from config import bcrypt


@jwt_required
def read_all():
    users = User.query.order_by(User.last_name).all()

    # Serialize the data for the response
    user_schema = UserSchema(many=True)
    data = user_schema.dump(users).data
    return data


@jwt_required
def read_one(user_id):
    user = (
        User.query.filter(User.id == user_id).one_or_none()
    )

    if user is not None:
        # Serialize the data for the response
        user_schema = UserSchema()
        data = user_schema.dump(user).data
        return data
    else:
        abort(404, "User not found for id: {}".format(user_id))


@jwt_required
def create(user):
    first_name = user.get("first_name")
    last_name = user.get("last_name")
    email = user.get("email")
    password = user.get("password")

    existing_user = (
        User.query.filter(User.email == email)
        .one_or_none()
    )

    # Can we insert this person?
    if existing_user is None:

        schema = UserSchema()
        new_user = schema.load(user, session=db.session).data
        new_user.password = bcrypt.generate_password_hash(password)

        db.session.add(new_user)
        db.session.commit()

        data = schema.dump(new_user).data

        return data, 201

    else:
        abort(409, "User with e-mail {} exists already".format(email))


@jwt_required
def update(user_id, user):
    update_user = User.query.filter(
        User.id == user_id
    ).one_or_none()

    if update_user is not None:
        schema = UserSchema()
        update = schema.load(user, session=db.session).data

        update.id = update_user.id
        update.password = bcrypt.generate_password_hash(user.get("password"))

        db.session.merge(update)
        db.session.commit()

        # return updated person in the response
        data = schema.dump(user).data

        return data, 200

    else:
        abort(404, "User not found for id: {}".format(user_id))


@jwt_required
def delete(user_id):
    user = User.query.filter(User.id == user_id).one_or_none()

    if user is not None:
        db.session.delete(user)
        db.session.commit()
        return make_response("User {} deleted".format(user_id), 200)

    else:
        abort(404, "User not found for id: {}".format(user_id))
